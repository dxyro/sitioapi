from django.urls import include, path
from rest_framework import routers
from .views import UserViewsSet


router = routers.DefaultRouter()
router.register('users', UserViewsSet)

urlpatterns = [
    path('', include(router.urls), name='apiurl'),
]